#!/bin/env bash
#安装expect
rpm -q expect &>/dev/null
if [ $? -ne 0 ];then
        yum install -y expect >/dev/null
        echo "expect安装成功"
fi
/usr/bin/expect << EOF
set timeout 30
spawn bash -c "mysql_secure_installation"
expect 	"*root (enter for none)*" {send "\r"}
expect	"*Set root password?*" {send "y\r"}
expect "*New password:*" {send "123456\r"}
expect "*Re-enter new password*" {send "123456\r"}
expect "*Remove anonymous users?*" {send "y\r"}
expect "*Disallow root login remotely?*" {send "n\r"}
expect "*Remove test database and access to it*" {send "y\r"}
expect "*Reload privilege tables now?*" {send "y\r"}
expect eof
EOF

